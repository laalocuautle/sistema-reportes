# Sistema

Sistema para registrar reportes de empleados

## Instrucciones
Usar composer para instalar dependencias de laravel y npm para dependencias de Vue (front)

```bash
composer install && yarn install
```
## Compilar dependencias Vue

```bash
yarn dev
```

## Ejecutar migraciones

```bash
php artisan migrate
```

### Crear datos fake para reportes y empleados

```bash
php artisan migrate:fresh --seed
```
Nota: en caso de que de error al ejecutar el seeder, se debera ejecutar lo siguiente:
```bash
composer dump-autoload
```
y volver a ejecutar 
```bash
php artisan migrate:fresh --seed
```

## Acceso al sistema como administrador
Una vez ejecutado el seeder se puede acceder al sistema con las siguientes credenciales

```bash
email : admin@admin.com
password : password

```