<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Report,User};

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $reports = auth()->user()->isAdmin() ? Report::latest()->paginate() : Report::Where(['user_id' => auth()->id()])->latest()->paginate();
        foreach ($reports as $report){
            $report->usuario = $report->user->username;
            $report->own = $report->isOwn();
        }
        $data = [
            'reports' => $reports,
            'users' => User::all()
        ];
        return view('home', $data);
    }
}
