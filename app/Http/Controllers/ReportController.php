<?php

namespace App\Http\Controllers;

use App\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->add(['user_id' => auth()->id()]);

        $validator = Validator::make($request->all(),[
            'description' => ['required', 'string', 'max:150'],
            'user_id' => ['required'],
        ],[
            'description.required' => 'La descripción es requerido.',
            'description.max' => 'La descripción debe de ser maximo 150 caracteres.',
            'user_id.required' => 'El usuario es requerido.',
        ]);

        if($validator->fails()){
            return response()->json(['success' => false,'errors' => $validator->errors()->all()]);
        }

        $report = Report::create($request->all());
        $report->usuario = $report->user->username;
        $report->own = $report->isOwn();
        return response()->json(['success' => true,'report' => $report]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
        $report->update($request->all());
        $report->usuario = $report->user->username;
        $report->own = $report->isOwn();
        return response()->json(['success' => true,'report' => $report]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        $report->delete();
        return response()->json(['success' => true]);
    }
}
