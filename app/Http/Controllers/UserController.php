<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\UserStoreRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth',['except' => ['login','register']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::Where('id','!=',auth()->id())->latest()->paginate();
        foreach ($users as $user){
            $user->typeRole = $user->role ?? 0;
            $user->role = $user->getRole();

        }
        $roles = User::roles();
        $data = [
            'users' => $users,
            'roles' => $roles
        ];

        return view('users.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'username' => ['required', 'string', 'max:25','unique:users'],
            'email' => ['required', 'string', 'email', 'max:50', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ],[
            'username.required' => 'El nombre de usuario es requerido.',
            'username.max' => 'El nombre de usuario debe de ser maximo 25 caracteres.',
            'username.unique' => 'El nombre de usuario ya se encuentra registrado',
            'email.required' => 'El campo Email es requerido',
            'email.max' => 'El campo Email debe de ser maximo 50 caracteres',
            'email.unique' => 'El campo Email ya se encuentra registrado',
            'password.required' => 'La Contraseña es requerido',
            'password.min' => 'La Contraseña debe ser mínimo de 8 caracteres'
        ]);
        if($validator->fails()){
            return response()->json(['success' => false,'errors' => $validator->errors()->all()]);
        }
        $employe = User::create($request->all());
        $employe = self::findModel($employe->id);
        $employe->typeRole = $employe->role ?? 0;
        $employe->role = $employe->getRole();
        return response()->json(['success' => true,'employe' => $employe]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = self::findModel($id);
        $model->update($request->all());
        $model->typeRole = $model->role ?? 0;
        $model->role = $model->getRole();
        return response()->json(['success' => true,'employe' => $model]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = self::findModel($id);
        $model->delete();
        return response()->json(['success' => true]);
    }

    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email,'password' => $request->password])){
            return redirect()->to('/');
        }else{
            return redirect()->back()->with(['error-credentials' => true]);
        }
    }

    public function register(UserStoreRequest $request)
    {
        $user = User::create($request->all());
        Auth::login($user);
        return redirect()->to('/');
    }

    private function findModel($id)
    {
        return User::findOrFail($id);
    }
}
