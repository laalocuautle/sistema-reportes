<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => ['required', 'string', 'max:25','unique:users'],
            'email' => ['required', 'string', 'email', 'max:50', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
    }

    public function messages()
    {
        return [
            'username.required' => 'El campo Usuario es requerido.',
            'username.max' => 'El campo Usuario debe de ser maximo 25 caracteres.',
            'username.unique' => 'El campo Usuario ya se encuentra registrado',
            'email.required' => 'El campo Email es requerido',
            'email.max' => 'El campo Email debe de ser maximo 50 caracteres',
            'email.unique' => 'El campo Email ya se encuentra registrado',
            'password.required' => 'La Contraseña es requerido',
            'password.min' => 'La Contraseña debe ser mínimo de 8 caracteres',
            'password.confirmed' => 'Las Contraseñas no coinciden'
        ];
    }
}
