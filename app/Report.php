<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = ['description','user_id'];

    protected $appends = ['creado_hace'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getCreadoHaceAttribute()
    {
        return (string) $this->created_at->diffForHumans();
    }

    public function isOwn()
    {
        return auth()->id() == $this->user_id || auth()->user()->isAdmin();
    }
}
