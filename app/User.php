<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
class User extends Authenticatable
{
    use Notifiable;

    const ADMIN = 1;
    const EMPLOYE = 0;

    private static $opcionesRole = [
        self::ADMIN => 'Administrador',
        self::EMPLOYE => 'Empleado'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password','role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    public function isAdmin()
    {
        return $this->role == self::ADMIN;
    }

    public function isEmploye()
    {
        return $this->role == self::EMPLOYE;
    }


    public function reports()
    {
        return $this->hasMany(Report::class);
    }

    public static function roles()
    {
        return self::$opcionesRole;
    }
    public function getRole()
    {
        return self::$opcionesRole[$this->role];
    }
}
