<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Report;
use Faker\Generator as Faker;

$factory->define(Report::class, function (Faker $faker) {
    return [
        'description' => $faker->realText(rand(100,140)),
        'created_at' => $faker->dateTimeBetween('-30 days','now')
    ];
});
