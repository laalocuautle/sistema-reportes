<?php

use Illuminate\Database\Seeder;
use App\{Report,User};
class ReportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class,10)->create()->each(function($user){
            $user->reports()->saveMany(factory(Report::class,rand(10,25))->make());
        });


        $admin = User::find(1);
        $admin->reports()->saveMany(factory(Report::class,rand(10,25))->make());
    }
}
