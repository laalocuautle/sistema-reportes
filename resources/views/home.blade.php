@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <reports-component :reports="{{$reports->toJson()}}" :users="{{$users->toJson()}}"/>
    </div>
    @include('sections.paginator',['modelo' => $reports])

</div>
@endsection
