@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <users-component :users="{{$users->toJson()}}" :roles="{{json_encode($roles)}}"/>
        </div>
        @include('sections.paginator',['modelo' => $users])
    </div>
@endsection
