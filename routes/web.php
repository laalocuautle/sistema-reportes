<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::post('/register','UserController@register')->name('user.register');
Route::post('/login','UserController@login')->name('user.login');
Route::get('/employes','UserController@index')->name('user.index');
Route::post('/employes','UserController@store');
Route::put('/employes/{id}','UserController@update');
Route::delete('/employes/{id}','UserController@destroy');

Route::post('/reports','ReportController@store');
Route::delete('/reports/{report}','ReportController@destroy');
Route::put('/reports/{report}','ReportController@update');

